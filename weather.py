from tkinter import  *
from tkinter import  messagebox
from configparser import ConfigParser
import requests

url="https://api.openweathermap.org/data/2.5/forecast?q={}&appid={}"

config_file='config.ini'
config= ConfigParser()
config.read(config_file)
api_key=config['api_key']['key']

def get_weather(city):
    result=requests.get(url.format(city,api_key))
    if result:
        json = result.json()
        city = json['city']['name']
        country = json['city']['country']
        temp_kelvin = json['list'][0]['main']['temp']
        temp_celsius = temp_kelvin-273.15
        temp_fahrenheit = (temp_kelvin-273.15) * 9 / 5 +32
        icon = json['list'][0]['weather'][0]['icon']
        weather = json['list'][0]['weather'][0]['main']
        more_date=json['list'][0]['dt_txt']
        more_icon=json['list'][0]['weather'][0]['icon']
        more_temp_max=json['list'][0]['main']['temp_max']-273.15
        more_temp_min=json['list'][0]['main']['temp_min']-273.15
       
        final = (city,country,temp_celsius,temp_fahrenheit,icon,weather,more_date,more_icon,more_temp_max,more_temp_min)
        return final
    else:
        return None

def search():
    
    city = city_text.get()
    weather = get_weather(city)
    if weather:
        location_lbl['text']=f"{weather[0]},{weather[1]}"
        # image['bitmap']='Weather_icon/{}.png'.format(weather[4])
        temp_lbl['text']='{:.2f}°C  {:.2f}°F'.format(weather[2],weather[3])
        weather_lbl['text']=weather[5]
        
        more_btn=Button(app,text="More Detail",font=20,command=more)
        more_btn.pack()
    else:
        messagebox.showerror('Error',f"Cannot Find Weather {city}, Please Enter Valid City")
        

def more():
    city = city_text.get()
    weather = get_weather(city)
    more_date_lbl['text']= f"Date Time {weather[6]}"
    more_temp_max_lbl['text']=f"Max_Temp {weather[8]}"
    more_temp_min_lbl['text']=f"Min_Temp {weather[9]}"

    
app=Tk()
app.title("Weather App")
app.geometry('800x400')

city_text=StringVar()
city_entry=Entry(app,textvariable=city_text,justify='center',font=("poppins", 25,))
city_entry.focus()
city_entry.pack()

search_btn=Button(app,text="search weather",font=20,command=search)
search_btn.pack()

location_lbl=Label(app,text="Location",font=("poppins", 30, "bold"))
location_lbl.pack()

image=Label(app,bitmap='',font=("poppins", 20, "bold"))
image.pack()

temp_lbl=Label(app,text='',font=("poppins", 20, "bold"))
temp_lbl.pack()

weather_lbl=Label(app,text='',font=("poppins", 20, "bold"))
weather_lbl.pack()

more_date_lbl=Label(app,text='',font=("poppins", 20, "bold"))
more_date_lbl.pack()

more_temp_max_lbl=Label(app,text='',font=("poppins", 20, "bold"))
more_temp_max_lbl.pack()

more_temp_min_lbl=Label(app,text='',font=("poppins", 20, "bold"))
more_temp_min_lbl.pack()

app.mainloop()